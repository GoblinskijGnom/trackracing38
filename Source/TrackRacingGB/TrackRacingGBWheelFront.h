// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "TrackRacingGBWheelFront.generated.h"

UCLASS()
class UTrackRacingGBWheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UTrackRacingGBWheelFront();
};



