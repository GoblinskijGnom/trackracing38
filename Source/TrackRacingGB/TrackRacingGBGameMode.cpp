// Copyright Epic Games, Inc. All Rights Reserved.

#include "TrackRacingGBGameMode.h"
#include "TrackRacingGBPawn.h"
#include "TrackRacingGBHud.h"

ATrackRacingGBGameMode::ATrackRacingGBGameMode()
{
	DefaultPawnClass = ATrackRacingGBPawn::StaticClass();
	HUDClass = ATrackRacingGBHud::StaticClass();
}
